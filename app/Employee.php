<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['first_name', 'last_name', 'age', 'country_id', 'avatar'];

    public $timestamps = false;

    public function country(){
        return $this->belongsTo('App\Country', 'country_id');
    }
}
