<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name'];

    public $timestamps = false;

    public function employee(){
        return $this->hasMany('App\Employee', 'country_id');
    }
}
