<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Employee;
use Yajra\DataTables\DataTables;
use Validator;
use Image;

class EmployeeController extends Controller
{
    public function index()
    {
        $countries = Country::all();
        return view('employee', compact('countries'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:50', 'regex:/^[a-zA-Z ]*$/'],
            'last_name' => ['required', 'string', 'max:50', 'regex:/^[a-zA-Z ]*$/'],
            'age' => ['required', 'numeric', 'max:100', 'min:18'],
            'country_id' => ['required', 'numeric', 'country'],
            'avatar' => ['required', 'image', 'max:2048', 'mimes:jpeg,png,jpg'],
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();

        //upload image plus resize
        if(isset($request->avatar)){
            $image = $request->avatar;
            $filename  = time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('avatar/' . $filename);
            Image::make($image->getRealPath())->fit(200)->save($path);
            $input['avatar'] = $filename;
        }
        
        $newItem = Employee::create($input);
        return response()->json(['success'=>$newItem], 200);
    }

    public function show($id)
    {
        $item = Employee::findOrFail($id);
        $item->country;
        return response()->json($item, 200);
    }

    //datatable handler
    public function datatable()
    {
        $parameters = Employee::with('country');
        return Datatables::of($parameters)
        ->addColumn('name', function($parameter){
            return $parameter->first_name.' '.$parameter->last_name;
        })
        ->addColumn('action', function($parameter){
            return '<td>'.
                '<button type="button" class="btn btn-sm btn-rounded btn-outline-primary" onClick="viewItem('.$parameter->id.')">'.'<i class="ti-info"></i> View</button>';
        })
        ->rawColumns(['name', 'action'])->make(true);
    }
}
