<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Country;

class ReportController extends Controller
{
    public function index()
    {
        return view('report');        
    }

    public function getValue(){
        $country_list = []; //list of country name
        $employee_count = []; //list of total employee per country
        $employee_min = []; //list of the youngest per country
        $employee_max = []; //list of the oldest per country
        $employee_avg = []; //list of average age per country
        $all_min = Employee::min('age'); //the youngest of all country
        $all_max = Employee::max('age'); //the oldest of all country
        $all_avg = Employee::avg('age'); //average age of all country
        $grand_total = Employee::count(); //total employee

        $countries = Country::select('name', 'id')->orderBy('name')->get(); //country objects
        
        //pupulate list so it can be consume by the chart
        foreach($countries as $country){
            $count = $country->employee->count();
            $min = $country->employee->min('age');
            $min = $country->employee->min('age');
            $max = $country->employee->max('age');
            $avg = $country->employee->avg('age');
            array_push($country_list, $country->name);
            array_push($employee_count, $count);
            array_push($employee_min, $min);
            array_push($employee_max, $max);
            array_push($employee_avg, intval($avg));
        }

        return response()->json([
            'all_min' => $all_min,
            'all_max' => $all_max,
            'all_avg' => intval($all_avg),
            'grand_total' => $grand_total,
            'country_list' => $country_list,
            'employee_count' => $employee_count,
            'employee_min' => $employee_min,
            'employee_max' => $employee_max,
            'employee_avg' => $employee_avg
        ],200);
    }
}
