//get chart value
$(function(){
    $.ajax({
        type: "GET",
        url: base_url+'/report/getvalue',
        dataType: 'json',
        success: function (data) {
            // console.log(data)
            $('#view-tot').text(data.grand_total);
            $('#view-min').text(data.all_min);
            $('#view-avg').text(data.all_avg);
            $('#view-max').text(data.all_max);
            pupulate(data.country_list, data.employee_count, data.employee_min, data.employee_max, data.employee_avg);
        },
        error: function(data) {
            swal({
                title: 'Oops...!',
                text: 'something wrong',
                type: 'error',
                timer: '800'
            })
        }
    });
});

//pupulate chart
function pupulate(country_list, employee_count, employee_min, employee_max, employee_avg){
    var ctx = document.getElementById('num-chart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: country_list,
            datasets: [{
                label: "The number of employees",
                backgroundColor: 'rgb(88, 103, 221)',
                borderColor: 'rgb(88, 103, 221)',
                data: employee_count,
                        fill: false,
                    }]
                },
        options: {
                responsive: true
            }
    });

    var ctx = document.getElementById('age-chart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: country_list,
            datasets: [{
                label: "Youngest",
                backgroundColor: 'rgb(67, 249, 195)',
                borderColor: 'rgb(67, 249, 195)',
                data: employee_min,
                        fill: false,
                    },{
                label: "Avg",
                backgroundColor: 'rgb(67, 127, 249)',
                borderColor: 'rgb(67, 127, 249)',
                data: employee_avg,
                        fill: false,
                    },{
                label: "Oldest",
                backgroundColor: 'rgb(249, 67, 67)',
                borderColor: 'rgb(249, 67, 67)',
                data: employee_max,
                        fill: false,
                    }]
                },
        options: {
                responsive: true
            }
    });
}
