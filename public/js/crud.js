function addItem() {
    $('#input-method').val('POST');
    $('#modal-form').modal('show');
    $('#modal-form form')[0].reset();

    $(".dis-error").text("");
    $(".dis-error").css("dislay","none");
}

function printErrorMsg (msg) {
    $(".dis-error").text("");
    $(".dis-error").css("display", "none");
    $.each( msg, function( key, value ) {
        $(".error_"+key).text(value[0]);
        $(".error_"+key).css("display","block");
    });
}

function viewItem(id) {
    $.ajax({
        type: "GET",
        url: base_url+'/employee/' + id,
        dataType: 'json',
        success: function (data) {
            $("#view-name").text(data.first_name+' '+data.last_name);
            $("#view-age").text(data.age);
            $("#view-country").text(data.country.name);
            $("#view-avatar").attr('src', base_url+'/avatar/'+data.avatar);
            $('#modal-show').modal('show');
        },
        error: function(data) {
            $('#modal-show').modal('toggle');
            swal({
                title: 'Oops...!',
                text: 'something wrong',
                type: 'error',
                timer: '800'
            })
        }
    });
}

//form submit handler
$(function(){
    $("#employee-form").submit(function (e) {
		e.preventDefault();
		
		$.ajax({
		    type: "POST",
			url: base_url+'/employee',
			cache: false,
			contentType: false,
			processData: false,
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
			data:  new FormData(this),
		    beforeSend: function() {
                $("#btn-close").prop("disabled", true);
                $("#btn-submit").prop("disabled", true);
		    	$("#btn-submit").text("Loading...");
		    }, statusCode: {
				500: function() {
					swal({
                        title: 'Oops...!',
                        text: 'error 500',
                        type: 'error',
                        timer: '1500'
                    })
				}
	    	}, success: function (data) {
					swal({
                        title: 'Success!',
                        text: 'success',
                        type: 'success',
                        timer: '1500'
                    })
					
                    $('#modal-form').modal('toggle');
                    table.ajax.reload();
		    }, error :function(data) {
				var res = data.responseJSON;
		        printErrorMsg(res.error);
	    	}, complete() {
				$("#btn-submit").text("Submit");
                $("#btn-submit").prop("disabled", false);
                $("#btn-close").prop("disabled", false);
	    	}
		});
    }); 
});