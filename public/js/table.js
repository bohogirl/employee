//ajax employee datatable
var table = $('#employee-table').DataTable({
  processing: true,
  serverSide: false,
  ajax: base_url+"/employee/table",
  columns: [
    {data: 'name', name: 'name'},
    {data: 'country.name', name: 'country.name'},
    {data: 'age', name: 'age'},
    {data: 'action', name: 'action', orderable: false, searchable: false}
  ]
});