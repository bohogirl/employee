@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-3 col-xs-6 m-b-3">
                <div class="card">
                    <div class="card-body"><span class="info-box-icon bg-aqua"><i class="icon-user"></i></span>
                        <div class="info-box-content"> <span id="view-tot" class="info-box-number"></span> <span class="info-box-text">Total Employee</span> </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6 m-b-3">
                <div class="card">
                    <div class="card-body"><span class="info-box-icon bg-green"><i class="icon-user"></i></span>
                        <div class="info-box-content"> <span id="view-min" class="info-box-number"></span> <span class="info-box-text">Youngest</span></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6 m-b-3">
                <div class="card">
                    <div class="card-body"><span class="info-box-icon bg-yellow"><i class="icon-user"></i></span>
                        <div class="info-box-content"> <span id="view-avg" class="info-box-number"></span> <span class="info-box-text">Avg Age</span></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6 m-b-3">
                <div class="card">
                    <div class="card-body"><span class="info-box-icon bg-red"><i class="icon-user"></i></span>
                        <div class="info-box-content"> <span id="view-max" class="info-box-number"></span> <span class="info-box-text">Oldest</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="info-box">
                    <div class="col-12">
                        <div class="d-flex flex-wrap">
                            <div>
                                <h5>Total each Country</h5>
                            </div>
                        </div>
                    </div>
                    <div>
                        <canvas id="num-chart"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="info-box">
                    <div class="col-12">
                        <div class="d-flex flex-wrap">
                            <div>
                            <h5>Age Chart</h5>
                            </div>
                        </div>
                    </div>
                    <div>
                        <canvas id="age-chart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('css')
    <!-- Chartist CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/plugins/chartist-js/chartist.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/plugins/chartist-js/chartist-plugin-tooltip.css') }}">
@endsection

@section('js')
    <!-- Chartjs JavaScript --> 
    <script src="{{ asset('vendor/plugins/chartjs/chart.min.js') }}"></script>
    <script src="{{ asset('js/chart.js') }}"></script>

@endsection
