@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">
                        <h4 class="text-black">Employee</h4>
                    </div>
                    <div class="pull-right">
                        <button type="button" class="btn btn-sm btn-rounded btn-outline-success" onclick="addItem()"><i class="ti-plus"></i> Add</button>
                    </div>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table id="employee-table" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Country</th>
                                    <th>Age</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('modal-form')
@include('modal-show')

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/plugins/datatables/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('js')
    <script src="{{ asset('vendor/plugins/datatables/jquery.dataTables.min.js') }}"></script> 
    <script src="{{ asset('vendor/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/table.js') }}"></script>
    <script src="{{ asset('js/crud.js') }}"></script>
@endsection
