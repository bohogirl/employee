<!-- Modal view employee -->
<div id="modal-show" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="card">
            <div class="card-body"> <img id="view-avatar" class="profile-user-img img-responsive img-circle m-b-2" src="" alt="User profile picture">
              <h3 class="profile-username text-center"><view id="view-name">Name</view></h3>
              <p class="text-center"><i class="fa fa-user-circle"></i> <view id="view-age">100</view> years old</p>
              <p class="text-center"><i class="fa fa-map-marker"></i> <view id="view-country">Country</view></p>
            </div>
          </div>
        </div>
    </div>
</div>
