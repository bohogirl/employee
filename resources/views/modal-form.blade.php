<!-- Modal Form Employee -->
<div id="modal-form" data-backdrop="static" data-keyboard="false" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="card">
                <div class="card-body">
                    <h4 id="modal-title">Form Lembaga</h4><p></p>
                    <form id="employee-form" class="form-horizontal ">
                        <div class="form-group row">
                            <label for="first_name" class="col-sm-3 control-label">First Name</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <input type="text" id="first_name" name="first_name" placeholder="e.g. John" class="form-control">
                                </div>
                                <p class="text-danger dis-error error_first_name" style="display:none"></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="last_name" class="col-sm-3 control-label">Last Name</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <input type="text" id="last_name" name="last_name" placeholder="e.g. Doe" class="form-control">
                                </div>
                                <p class="text-danger dis-error error_last_name" style="display:none"></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="country_id" class="col-sm-3 control-label">Country</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <select id="country_id" name="country_id" class="form-control">
                                        <option value="0">Select Country</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <p class="text-danger dis-error error_country_id" style="display:none"></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="age" class="col-sm-3 control-label">Age</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <input type="number" id="age" name="age" placeholder="e.g. 23" class="form-control">
                                </div>
                                <p class="text-danger dis-error error_age" style="display:none"></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="avatar" class="col-sm-3 control-label">Last Name</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <input type="file" id="avatar" name="avatar" placeholder="avatar" class="form-control">
                                </div>
                                <p class="text-danger dis-error error_avatar" style="display:none"></p>
                            </div>
                        </div>
                        <div class="form-group row m-b-0">
                            <div class="offset-sm-3 col-sm-9">
                                <div class="pull-right">
                                    <button id="btn-close" type="submit" class="btn btn-sm btn-rounded btn-light waves-effect waves-light" data-dismiss="modal">Close</button>&nbsp
                                    <button id="btn-submit" type="submit" class="btn btn-sm btn-rounded btn-outline-success waves-effect waves-light">Sumbit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
