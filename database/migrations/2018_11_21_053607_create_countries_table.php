<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->String('name', 50);
        });

        //insert country
        DB::table('countries')->insert([
            ['name'=>'Indonesia'],
            ['name'=>'Australia'],
            ['name'=>'Malaysia'],
            ['name'=>'Singapore'],
            ['name'=>'Philippine'],
            ['name'=>'Timor Leste']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
