## Installation
Make sure you have composer installed, please see [get composer](https://getcomposer.org/).

#### 1. Install dependencies
```bash
composer install
```

#### 2. Create .env file
Create .env file by copying the .env.exampple
```bash
cp .env.example .env
```

#### 3. Configuration
Create new sql database, and setup inside .env file or config/database.php. 
Set the app url inside .env file or config/app.php

#### 4. Generate app key
```bash
php artisan key:generate
```

#### 5. Migrate database
```bash
php artisan migrate
```

#### 6. Run
```bash
php artisan serve
```
or open (your-project-url)/public
